# eCOM Project - MyCamping

## Members

* Samuel BAMBA
* Sekina BELGUENDOUZ
* Servan CHARLOT
* Florian CUZIN
* Timothée DEPRIESTER

## Description

The eCOM project aims to design and develop a web application.
More details [here](https://air.imag.fr/index.php/ECOM).

## Tasks

You can find the current progression of the project [here](https://trello.com/b/EGx6BUVB/ecom-mycamping).

## Version

**Version :** 1.1.0
For more details, please check the [release notes](Documentation/Release_notes.md).
