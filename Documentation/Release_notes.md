# Release Notes  

## Version 1.1.0 *(Minor Version)*  

\+ Pages' Layout adapting to screens.  
\+ API request in all the page.  
\+ Adding the accomodation page.  

## Version 1.0.0 *(Major Version)*  

\+ Merge FrontEnd BackEnd.  

## Version 0.8.0 *(Minor Version)*  

\+ API Filter by All.  

## Version 0.7.1 *(Patch)*  

\+ API routing permission.  

## Version 0.7.0 *(Minor Version)*  

\+ API Filter by Dates.  
\+ API Filter by Utilities.  
\+ API Filter by Type.  
\+ API Filter by Location.  
\+ API Filter by Surface.  
\+ API Filter by Price.  

## Version 0.6.0 *(Minor Version)*  

\+ Adding mail service.  
\+ Fixing security issue.  

## Version 0.5.0 *(Minor Version)*  

\+ Adding the payment page.  

## Version 0.4.1 *(Patch)*  

\+ Adding Watchtower.  

## Version 0.4.0 *(Minor Version)*  

\+ Application working in production.  

## Version 0.3.0 *(Minor Version)*  

\+ Database generated.  
\+ API Swagger generated.  
\+ Creating the home page, header and footer (responsive).  

## Version 0.2.1 *(Patch)*  

\+ SSL securitation of traefik.  
\+ Added compatible labels with traefik.  

## Version 0.2.0 *(Minor Version)*  

\+ GitLab CI integration.  

## Version 0.1.1 *(Patch)*  

\+ Updating Jhipster to version 5.4.2.  

## Version 0.1.0 *(Minor Version)*  

\+ Vanilla Jhipster generation.  

\+ Additions  
\- Deletions  
  
[Test Results](/Documentation/Tests_results/version_0.0.x.txt) (Only for major and minor versions).  
