# Commit Formats

Each commit *MUST* have one of the following prefixes : 

`ADD: <message>` - Addition: A folder, file, text within a file has been added.  
`DEL: <message>` - Deletion: A folder, file, text within a file has been deleted.  
`FIX: <message>` - Fix : A mistake or a bug has been fixed or is being fixed.  
`REF: <message>` - Refactoring : A small optimisation, change has been done.  