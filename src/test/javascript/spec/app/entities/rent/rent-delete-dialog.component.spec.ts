/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MycampingTestModule } from '../../../test.module';
import { RentDeleteDialogComponent } from 'app/entities/rent/rent-delete-dialog.component';
import { RentService } from 'app/entities/rent/rent.service';

describe('Component Tests', () => {
    describe('Rent Management Delete Component', () => {
        let comp: RentDeleteDialogComponent;
        let fixture: ComponentFixture<RentDeleteDialogComponent>;
        let service: RentService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MycampingTestModule],
                declarations: [RentDeleteDialogComponent]
            })
                .overrideTemplate(RentDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RentDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RentService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
