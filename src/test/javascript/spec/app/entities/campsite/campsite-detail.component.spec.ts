/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MycampingTestModule } from '../../../test.module';
import { CampsiteDetailComponent } from 'app/entities/campsite/campsite-detail.component';
import { Campsite } from 'app/shared/model/campsite.model';

describe('Component Tests', () => {
    describe('Campsite Management Detail Component', () => {
        let comp: CampsiteDetailComponent;
        let fixture: ComponentFixture<CampsiteDetailComponent>;
        const route = ({ data: of({ campsite: new Campsite(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MycampingTestModule],
                declarations: [CampsiteDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CampsiteDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CampsiteDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.campsite).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
