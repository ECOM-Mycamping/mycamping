/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MycampingTestModule } from '../../../test.module';
import { CampsiteComponent } from 'app/entities/campsite/campsite.component';
import { CampsiteService } from 'app/entities/campsite/campsite.service';
import { Campsite } from 'app/shared/model/campsite.model';

describe('Component Tests', () => {
    describe('Campsite Management Component', () => {
        let comp: CampsiteComponent;
        let fixture: ComponentFixture<CampsiteComponent>;
        let service: CampsiteService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MycampingTestModule],
                declarations: [CampsiteComponent],
                providers: []
            })
                .overrideTemplate(CampsiteComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CampsiteComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampsiteService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Campsite(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.campsites[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
