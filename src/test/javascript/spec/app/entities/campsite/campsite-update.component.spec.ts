/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { MycampingTestModule } from '../../../test.module';
import { CampsiteUpdateComponent } from 'app/entities/campsite/campsite-update.component';
import { CampsiteService } from 'app/entities/campsite/campsite.service';
import { Campsite } from 'app/shared/model/campsite.model';

describe('Component Tests', () => {
    describe('Campsite Management Update Component', () => {
        let comp: CampsiteUpdateComponent;
        let fixture: ComponentFixture<CampsiteUpdateComponent>;
        let service: CampsiteService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [MycampingTestModule],
                declarations: [CampsiteUpdateComponent]
            })
                .overrideTemplate(CampsiteUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CampsiteUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampsiteService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Campsite(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campsite = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Campsite();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campsite = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
