package edu.ecom.mycamping.web.rest;

import edu.ecom.mycamping.MycampingApp;

import edu.ecom.mycamping.domain.Campsite;
import edu.ecom.mycamping.repository.CampsiteRepository;
import edu.ecom.mycamping.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static edu.ecom.mycamping.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CampsiteResource REST controller.
 *
 * @see CampsiteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MycampingApp.class)
public class CampsiteResourceIntTest {

    private static final String DEFAULT_CAMPING = "AAAAAAAAAA";
    private static final String UPDATED_CAMPING = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMBER = 1;
    private static final Integer UPDATED_NUMBER = 2;

    private static final Integer DEFAULT_SURFACE = 1;
    private static final Integer UPDATED_SURFACE = 2;

    private static final String DEFAULT_UTILITIES = "AAAAAAAAAA";
    private static final String UPDATED_UTILITIES = "BBBBBBBBBB";

    private static final Float DEFAULT_PRICE = 1F;
    private static final Float UPDATED_PRICE = 2F;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Float DEFAULT_SUCC_DISCOUNT = 1F;
    private static final Float UPDATED_SUCC_DISCOUNT = 2F;

    @Autowired
    private CampsiteRepository campsiteRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCampsiteMockMvc;

    private Campsite campsite;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampsiteResource campsiteResource = new CampsiteResource(campsiteRepository);
        this.restCampsiteMockMvc = MockMvcBuilders.standaloneSetup(campsiteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campsite createEntity(EntityManager em) {
        Campsite campsite = new Campsite()
            .camping(DEFAULT_CAMPING)
            .location(DEFAULT_LOCATION)
            .type(DEFAULT_TYPE)
            .number(DEFAULT_NUMBER)
            .surface(DEFAULT_SURFACE)
            .utilities(DEFAULT_UTILITIES)
            .price(DEFAULT_PRICE)
            .description(DEFAULT_DESCRIPTION)
            .succDiscount(DEFAULT_SUCC_DISCOUNT);
        return campsite;
    }

    @Before
    public void initTest() {
        campsite = createEntity(em);
    }

    @Test
    @Transactional
    public void createCampsite() throws Exception {
        int databaseSizeBeforeCreate = campsiteRepository.findAll().size();

        // Create the Campsite
        restCampsiteMockMvc.perform(post("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campsite)))
            .andExpect(status().isCreated());

        // Validate the Campsite in the database
        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeCreate + 1);
        Campsite testCampsite = campsiteList.get(campsiteList.size() - 1);
        assertThat(testCampsite.getCamping()).isEqualTo(DEFAULT_CAMPING);
        assertThat(testCampsite.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testCampsite.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testCampsite.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testCampsite.getSurface()).isEqualTo(DEFAULT_SURFACE);
        assertThat(testCampsite.getUtilities()).isEqualTo(DEFAULT_UTILITIES);
        assertThat(testCampsite.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testCampsite.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCampsite.getSuccDiscount()).isEqualTo(DEFAULT_SUCC_DISCOUNT);
    }

    @Test
    @Transactional
    public void createCampsiteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campsiteRepository.findAll().size();

        // Create the Campsite with an existing ID
        campsite.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampsiteMockMvc.perform(post("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campsite)))
            .andExpect(status().isBadRequest());

        // Validate the Campsite in the database
        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLocationIsRequired() throws Exception {
        int databaseSizeBeforeTest = campsiteRepository.findAll().size();
        // set the field null
        campsite.setLocation(null);

        // Create the Campsite, which fails.

        restCampsiteMockMvc.perform(post("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campsite)))
            .andExpect(status().isBadRequest());

        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = campsiteRepository.findAll().size();
        // set the field null
        campsite.setType(null);

        // Create the Campsite, which fails.

        restCampsiteMockMvc.perform(post("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campsite)))
            .andExpect(status().isBadRequest());

        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSurfaceIsRequired() throws Exception {
        int databaseSizeBeforeTest = campsiteRepository.findAll().size();
        // set the field null
        campsite.setSurface(null);

        // Create the Campsite, which fails.

        restCampsiteMockMvc.perform(post("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campsite)))
            .andExpect(status().isBadRequest());

        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = campsiteRepository.findAll().size();
        // set the field null
        campsite.setPrice(null);

        // Create the Campsite, which fails.

        restCampsiteMockMvc.perform(post("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campsite)))
            .andExpect(status().isBadRequest());

        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCampsites() throws Exception {
        // Initialize the database
        campsiteRepository.saveAndFlush(campsite);

        // Get all the campsiteList
        restCampsiteMockMvc.perform(get("/api/campsites?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campsite.getId().intValue())))
            .andExpect(jsonPath("$.[*].camping").value(hasItem(DEFAULT_CAMPING.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].surface").value(hasItem(DEFAULT_SURFACE)))
            .andExpect(jsonPath("$.[*].utilities").value(hasItem(DEFAULT_UTILITIES.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].succDiscount").value(hasItem(DEFAULT_SUCC_DISCOUNT.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getCampsite() throws Exception {
        // Initialize the database
        campsiteRepository.saveAndFlush(campsite);

        // Get the campsite
        restCampsiteMockMvc.perform(get("/api/campsites/{id}", campsite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campsite.getId().intValue()))
            .andExpect(jsonPath("$.camping").value(DEFAULT_CAMPING.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.surface").value(DEFAULT_SURFACE))
            .andExpect(jsonPath("$.utilities").value(DEFAULT_UTILITIES.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.succDiscount").value(DEFAULT_SUCC_DISCOUNT.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCampsite() throws Exception {
        // Get the campsite
        restCampsiteMockMvc.perform(get("/api/campsites/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCampsite() throws Exception {
        // Initialize the database
        campsiteRepository.saveAndFlush(campsite);

        int databaseSizeBeforeUpdate = campsiteRepository.findAll().size();

        // Update the campsite
        Campsite updatedCampsite = campsiteRepository.findById(campsite.getId()).get();
        // Disconnect from session so that the updates on updatedCampsite are not directly saved in db
        em.detach(updatedCampsite);
        updatedCampsite
            .camping(UPDATED_CAMPING)
            .location(UPDATED_LOCATION)
            .type(UPDATED_TYPE)
            .number(UPDATED_NUMBER)
            .surface(UPDATED_SURFACE)
            .utilities(UPDATED_UTILITIES)
            .price(UPDATED_PRICE)
            .description(UPDATED_DESCRIPTION)
            .succDiscount(UPDATED_SUCC_DISCOUNT);

        restCampsiteMockMvc.perform(put("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCampsite)))
            .andExpect(status().isOk());

        // Validate the Campsite in the database
        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeUpdate);
        Campsite testCampsite = campsiteList.get(campsiteList.size() - 1);
        assertThat(testCampsite.getCamping()).isEqualTo(UPDATED_CAMPING);
        assertThat(testCampsite.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testCampsite.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testCampsite.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testCampsite.getSurface()).isEqualTo(UPDATED_SURFACE);
        assertThat(testCampsite.getUtilities()).isEqualTo(UPDATED_UTILITIES);
        assertThat(testCampsite.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testCampsite.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCampsite.getSuccDiscount()).isEqualTo(UPDATED_SUCC_DISCOUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingCampsite() throws Exception {
        int databaseSizeBeforeUpdate = campsiteRepository.findAll().size();

        // Create the Campsite

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampsiteMockMvc.perform(put("/api/campsites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campsite)))
            .andExpect(status().isBadRequest());

        // Validate the Campsite in the database
        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCampsite() throws Exception {
        // Initialize the database
        campsiteRepository.saveAndFlush(campsite);

        int databaseSizeBeforeDelete = campsiteRepository.findAll().size();

        // Get the campsite
        restCampsiteMockMvc.perform(delete("/api/campsites/{id}", campsite.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Campsite> campsiteList = campsiteRepository.findAll();
        assertThat(campsiteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Campsite.class);
        Campsite campsite1 = new Campsite();
        campsite1.setId(1L);
        Campsite campsite2 = new Campsite();
        campsite2.setId(campsite1.getId());
        assertThat(campsite1).isEqualTo(campsite2);
        campsite2.setId(2L);
        assertThat(campsite1).isNotEqualTo(campsite2);
        campsite1.setId(null);
        assertThat(campsite1).isNotEqualTo(campsite2);
    }
}
