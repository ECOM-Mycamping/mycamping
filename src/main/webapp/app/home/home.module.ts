import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MycampingSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [MycampingSharedModule, RouterModule.forChild([HOME_ROUTE]), NgMultiSelectDropDownModule, HttpClientModule],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MycampingHomeModule {}
