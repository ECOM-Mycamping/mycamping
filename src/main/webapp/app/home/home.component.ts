import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { LoginModalService, Principal, Account } from 'app/core';
import { Router } from '@angular/router';
import { faCalendar, faAngleDown, faAngleUp, faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';

import { ICampsite } from 'app/shared/model/campsite.model';
import { CampsiteService } from '../entities/campsite/campsite.service';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
    campsites: ICampsite[];
    displayImage: boolean;
    displayMore = 'Plus de filtres';
    account: Account;
    formSearch = {
        place: '',
        dateStart: null,
        dateEnd: null,
        priceMax: null,
        priceMin: null,
        surfaceMax: null,
        surfaceMin: null,
        selectedUtility: [],
        selectedType: []
    };
    faCalendar = faCalendar;
    faArrow = faArrowCircleRight;
    faAngle = faAngleDown;
    modalRef: NgbModalRef;

    typeSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: false
    };
    utilitySettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        itemsShowLimit: 3,
        limitSelection: 3,
        enableCheckAll: false,
        allowSearchFilter: false
    };

    // Items selected on our list
    utilityList = [
        { item_id: 1, item_text: 'Piscine' },
        { item_id: 2, item_text: 'Plage' },
        { item_id: 3, item_text: 'Terrain de sport' },
        { item_id: 4, item_text: 'Activité' },
        { item_id: 5, item_text: 'Electicité' },
        { item_id: 6, item_text: 'Sanitaire' },
        { item_id: 7, item_text: 'Buanderie' },
        { item_id: 8, item_text: 'Aire de jeu pour enfants' },
        { item_id: 9, item_text: 'Repas avec les hôtes' }
    ];

    typeList = [
        { item_id: 1, item_text: 'Terrain' },
        { item_id: 2, item_text: 'Caravane' },
        { item_id: 3, item_text: 'Bungalow' },
        { item_id: 4, item_text: 'Mobilhome' },
        { item_id: 5, item_text: 'Camping-Car' }
    ];

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private campsiteService: CampsiteService,
        private jhiAlertService: JhiAlertService,
        private router: Router
    ) {
        this.displayImage = true;
        this.campsites = [];
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
    }

    onDisplayClass(first: boolean) {
        if (this.displayImage) {
            if (first) {
                return 'home';
            } else {
                return 'search';
            }
        } else {
            return 'list';
        }
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.principal.identity().then(account => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    onClickMore() {
        if (this.displayMore === 'Plus de filtres') {
            this.faAngle = faAngleUp;
            this.displayMore = 'Moins de filtres';
        } else {
            this.faAngle = faAngleDown;
            this.displayMore = 'Plus de filtres';
        }
    }

    onRechercher() {
        this.displayImage = false;
        this.getCatalogue();
    }

    onClick(id: number) {
        if (this.formSearch.dateStart === null || this.formSearch.dateEnd === null) {
            this.router.navigate(['/accomodation/' + id.toString()]);
        } else {
            this.router.navigate(['/accomodation/' + id.toString()], {
                queryParams: { date: true, dateStart: this.formSearch.dateStart._i, dateEnd: this.formSearch.dateEnd._i }
            });
        }
    }

    getCatalogue() {
        this.campsiteService.filterByAll(this.formSearch).subscribe(
            (res: HttpResponse<ICampsite[]>) => {
                this.campsites = res.body;
            },
            (res: HttpErrorResponse) => this.jhiAlertService.error(res.message, null, null)
        );
    }
}
