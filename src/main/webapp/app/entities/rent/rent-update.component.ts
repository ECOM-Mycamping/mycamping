import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IRent } from 'app/shared/model/rent.model';
import { RentService } from './rent.service';
import { ICampsite } from 'app/shared/model/campsite.model';
import { CampsiteService } from 'app/entities/campsite';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client';

@Component({
    selector: 'jhi-rent-update',
    templateUrl: './rent-update.component.html'
})
export class RentUpdateComponent implements OnInit {
    private _rent: IRent;
    isSaving: boolean;

    campsites: ICampsite[];

    clients: IClient[];
    startDateDp: any;
    endDateDp: any;

    constructor(
        private jhiAlertService: JhiAlertService,
        private rentService: RentService,
        private campsiteService: CampsiteService,
        private clientService: ClientService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ rent }) => {
            this.rent = rent;
        });
        this.campsiteService.query().subscribe(
            (res: HttpResponse<ICampsite[]>) => {
                this.campsites = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.clientService.query().subscribe(
            (res: HttpResponse<IClient[]>) => {
                this.clients = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.rent.id !== undefined) {
            this.subscribeToSaveResponse(this.rentService.update(this.rent));
        } else {
            this.subscribeToSaveResponse(this.rentService.create(this.rent));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRent>>) {
        result.subscribe((res: HttpResponse<IRent>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCampsiteById(index: number, item: ICampsite) {
        return item.id;
    }

    trackClientById(index: number, item: IClient) {
        return item.id;
    }
    get rent() {
        return this._rent;
    }

    set rent(rent: IRent) {
        this._rent = rent;
    }
}
