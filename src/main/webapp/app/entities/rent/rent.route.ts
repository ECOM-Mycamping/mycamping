import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Rent } from 'app/shared/model/rent.model';
import { RentService } from './rent.service';
import { RentComponent } from './rent.component';
import { RentDetailComponent } from './rent-detail.component';
import { RentUpdateComponent } from './rent-update.component';
import { RentDeletePopupComponent } from './rent-delete-dialog.component';
import { IRent } from 'app/shared/model/rent.model';

@Injectable({ providedIn: 'root' })
export class RentResolve implements Resolve<IRent> {
    constructor(private service: RentService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((rent: HttpResponse<Rent>) => rent.body));
        }
        return of(new Rent());
    }
}

export const rentRoute: Routes = [
    {
        path: 'rent',
        component: RentComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.rent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rent/:id/view',
        component: RentDetailComponent,
        resolve: {
            rent: RentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.rent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rent/new',
        component: RentUpdateComponent,
        resolve: {
            rent: RentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.rent.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'rent/:id/edit',
        component: RentUpdateComponent,
        resolve: {
            rent: RentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.rent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const rentPopupRoute: Routes = [
    {
        path: 'rent/:id/delete',
        component: RentDeletePopupComponent,
        resolve: {
            rent: RentResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.rent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
