import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MycampingSharedModule } from 'app/shared';
import {
    RentComponent,
    RentDetailComponent,
    RentUpdateComponent,
    RentDeletePopupComponent,
    RentDeleteDialogComponent,
    rentRoute,
    rentPopupRoute
} from './';

const ENTITY_STATES = [...rentRoute, ...rentPopupRoute];

@NgModule({
    imports: [MycampingSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [RentComponent, RentDetailComponent, RentUpdateComponent, RentDeleteDialogComponent, RentDeletePopupComponent],
    entryComponents: [RentComponent, RentUpdateComponent, RentDeleteDialogComponent, RentDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MycampingRentModule {}
