import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRent } from 'app/shared/model/rent.model';
import { Principal } from 'app/core';
import { RentService } from './rent.service';

@Component({
    selector: 'jhi-rent',
    templateUrl: './rent.component.html'
})
export class RentComponent implements OnInit, OnDestroy {
    rents: IRent[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private rentService: RentService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.rentService.query().subscribe(
            (res: HttpResponse<IRent[]>) => {
                this.rents = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInRents();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IRent) {
        return item.id;
    }

    registerChangeInRents() {
        this.eventSubscriber = this.eventManager.subscribe('rentListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
