import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRent } from 'app/shared/model/rent.model';

type EntityResponseType = HttpResponse<IRent>;
type EntityArrayResponseType = HttpResponse<IRent[]>;

@Injectable({ providedIn: 'root' })
export class RentService {
    private resourceUrl = SERVER_API_URL + 'api/rents';

    constructor(private http: HttpClient) {}

    create(rent: IRent): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(rent);
        return this.http
            .post<IRent>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(rent: IRent): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(rent);
        return this.http
            .put<IRent>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IRent>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IRent[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(rent: IRent): IRent {
        const copy: IRent = Object.assign({}, rent, {
            startDate: rent.startDate != null && rent.startDate.isValid() ? rent.startDate.format(DATE_FORMAT) : null,
            endDate: rent.endDate != null && rent.endDate.isValid() ? rent.endDate.format(DATE_FORMAT) : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
        res.body.endDate = res.body.endDate != null ? moment(res.body.endDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((rent: IRent) => {
            rent.startDate = rent.startDate != null ? moment(rent.startDate) : null;
            rent.endDate = rent.endDate != null ? moment(rent.endDate) : null;
        });
        return res;
    }
}
