import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MycampingClientModule } from './client/client.module';
import { MycampingOwnerModule } from './owner/owner.module';
import { MycampingCampsiteModule } from './campsite/campsite.module';
import { MycampingDiscountModule } from './discount/discount.module';
import { MycampingRentModule } from './rent/rent.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        MycampingClientModule,
        MycampingOwnerModule,
        MycampingCampsiteModule,
        MycampingDiscountModule,
        MycampingRentModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MycampingEntityModule {}
