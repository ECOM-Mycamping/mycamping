import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICampsite } from 'app/shared/model/campsite.model';
import { Principal } from 'app/core';
import { CampsiteService } from './campsite.service';

@Component({
    selector: 'jhi-campsite',
    templateUrl: './campsite.component.html'
})
export class CampsiteComponent implements OnInit, OnDestroy {
    campsites: ICampsite[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private campsiteService: CampsiteService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.campsiteService.query().subscribe(
            (res: HttpResponse<ICampsite[]>) => {
                this.campsites = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCampsites();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICampsite) {
        return item.id;
    }

    registerChangeInCampsites() {
        this.eventSubscriber = this.eventManager.subscribe('campsiteListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
