import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ICampsite } from 'app/shared/model/campsite.model';
import { CampsiteService } from './campsite.service';
import { IOwner } from 'app/shared/model/owner.model';
import { OwnerService } from 'app/entities/owner';

@Component({
    selector: 'jhi-campsite-update',
    templateUrl: './campsite-update.component.html'
})
export class CampsiteUpdateComponent implements OnInit {
    campsite: ICampsite;
    isSaving: boolean;

    owners: IOwner[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private campsiteService: CampsiteService,
        private ownerService: OwnerService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ campsite }) => {
            this.campsite = campsite;
        });
        this.ownerService.query().subscribe(
            (res: HttpResponse<IOwner[]>) => {
                this.owners = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.campsite.id !== undefined) {
            this.subscribeToSaveResponse(this.campsiteService.update(this.campsite));
        } else {
            this.subscribeToSaveResponse(this.campsiteService.create(this.campsite));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICampsite>>) {
        result.subscribe((res: HttpResponse<ICampsite>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackOwnerById(index: number, item: IOwner) {
        return item.id;
    }
}
