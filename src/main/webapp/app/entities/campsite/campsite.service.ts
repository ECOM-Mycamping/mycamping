import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICampsite } from 'app/shared/model/campsite.model';

type EntityResponseType = HttpResponse<ICampsite>;
type EntityArrayResponseType = HttpResponse<ICampsite[]>;

@Injectable({ providedIn: 'root' })
export class CampsiteService {
    private resourceUrl = SERVER_API_URL + 'api/campsites';

    constructor(private http: HttpClient) {}

    create(campsite: ICampsite): Observable<EntityResponseType> {
        return this.http.post<ICampsite>(this.resourceUrl, campsite, { observe: 'response' });
    }

    update(campsite: ICampsite): Observable<EntityResponseType> {
        return this.http.put<ICampsite>(this.resourceUrl, campsite, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICampsite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICampsite[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    filterByAll(form: any): Observable<EntityArrayResponseType> {
        const req = {
            dateDebut: form.dateDebut,
            dateFin: form.dateFin,
            utility1: form.utility1,
            utility2: form.utility2,
            utility3: form.utility3,
            priceMin: form.priceMin,
            priceMax: form.priceMax,
            surfaceMin: form.surfaceMin,
            surfaceMax: form.surfaceMax
        };
        Object.keys(req).forEach(key => {
            if (req[key] === undefined || req[key] === null) {
                delete req[key];
            }
        });
        const options = createRequestOption(req);
        return this.http.get<ICampsite[]>(this.resourceUrl + '/filterByAll', { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
