import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Campsite } from 'app/shared/model/campsite.model';
import { CampsiteService } from './campsite.service';
import { CampsiteComponent } from './campsite.component';
import { CampsiteDetailComponent } from './campsite-detail.component';
import { CampsiteUpdateComponent } from './campsite-update.component';
import { CampsiteDeletePopupComponent } from './campsite-delete-dialog.component';
import { ICampsite } from 'app/shared/model/campsite.model';

@Injectable({ providedIn: 'root' })
export class CampsiteResolve implements Resolve<ICampsite> {
    constructor(private service: CampsiteService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((campsite: HttpResponse<Campsite>) => campsite.body));
        }
        return of(new Campsite());
    }
}

export const campsiteRoute: Routes = [
    {
        path: 'campsite',
        component: CampsiteComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.campsite.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'campsite/:id/view',
        component: CampsiteDetailComponent,
        resolve: {
            campsite: CampsiteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.campsite.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'campsite/new',
        component: CampsiteUpdateComponent,
        resolve: {
            campsite: CampsiteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.campsite.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'campsite/:id/edit',
        component: CampsiteUpdateComponent,
        resolve: {
            campsite: CampsiteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.campsite.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campsitePopupRoute: Routes = [
    {
        path: 'campsite/:id/delete',
        component: CampsiteDeletePopupComponent,
        resolve: {
            campsite: CampsiteResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'mycampingApp.campsite.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
