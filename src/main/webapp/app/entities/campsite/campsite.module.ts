import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MycampingSharedModule } from 'app/shared';
import {
    CampsiteComponent,
    CampsiteDetailComponent,
    CampsiteUpdateComponent,
    CampsiteDeletePopupComponent,
    CampsiteDeleteDialogComponent,
    campsiteRoute,
    campsitePopupRoute
} from './';

const ENTITY_STATES = [...campsiteRoute, ...campsitePopupRoute];

@NgModule({
    imports: [MycampingSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CampsiteComponent,
        CampsiteDetailComponent,
        CampsiteUpdateComponent,
        CampsiteDeleteDialogComponent,
        CampsiteDeletePopupComponent
    ],
    entryComponents: [CampsiteComponent, CampsiteUpdateComponent, CampsiteDeleteDialogComponent, CampsiteDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MycampingCampsiteModule {}
