import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICampsite } from 'app/shared/model/campsite.model';

@Component({
    selector: 'jhi-campsite-detail',
    templateUrl: './campsite-detail.component.html'
})
export class CampsiteDetailComponent implements OnInit {
    campsite: ICampsite;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ campsite }) => {
            this.campsite = campsite;
        });
    }

    previousState() {
        window.history.back();
    }
}
