import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatter } from './shared/util/datepicker-adapter';
import { Ng2Webstorage, LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { MycampingSharedModule } from 'app/shared';
import { MycampingCoreModule } from 'app/core';
import { MycampingAppRoutingModule } from './app-routing.module';
import { MycampingHomeModule } from './home/home.module';
import { MycampingAccountModule } from './account/account.module';
import { MycampingEntityModule } from './entities/entity.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as moment from 'moment';
import { JhiMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { PaymentComponent } from './payment/payment.component';
import { MycampingPaymentModule } from './payment/payment.module';
import { AccomodationComponent } from './accomodation/accomodation.component';
import { MycampingAccomodationModule } from './accomodation/accomodation.module';

@NgModule({
    imports: [
        BrowserModule,
        MycampingAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        MycampingSharedModule,
        MycampingCoreModule,
        MycampingHomeModule,
        MycampingAccountModule,
        MycampingEntityModule,
        MycampingPaymentModule,
        FontAwesomeModule,
        MycampingAccomodationModule
    ],
    declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [LocalStorageService, SessionStorageService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: NgbDateParserFormatter,
            useClass: NgbDateFRParserFormatter
        }
    ],
    bootstrap: [JhiMainComponent]
})
export class MycampingAppModule {
    constructor(private dpConfig: NgbDatepickerConfig) {
        this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
    }
}
