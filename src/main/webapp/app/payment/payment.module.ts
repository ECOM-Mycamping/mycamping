import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MycampingSharedModule } from 'app/shared';
import { PAYMENT_ROUTE, PaymentComponent } from './';

@NgModule({
    imports: [MycampingSharedModule, RouterModule.forChild([PAYMENT_ROUTE])],
    declarations: [PaymentComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MycampingPaymentModule {}
