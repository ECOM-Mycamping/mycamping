import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiAlertService } from 'ng-jhipster';
import { Location } from '@angular/common';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { CampsiteService } from '../entities/campsite/campsite.service';
import { ICampsite } from 'app/shared/model/campsite.model';
import { ClientService } from '../entities/client/client.service';
import { IClient } from 'app/shared/model/client.model';
import { faCalendar, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
// import { faPaypal } from '@fortawesome/free-brands-svg-icons';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
    selector: 'jhi-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['payment.scss']
})
export class PaymentComponent implements OnInit {
    faCalendar = faCalendar;
    faArrow = faArrowCircleLeft;

    id: number;
    accomodation: ICampsite;
    dateEnd: Moment;
    dateStart: Moment;

    client = {
        lastName: null,
        firstName: null,
        mail: null,
        phone: null,
        username: null,
        password: null
    };

    constructor(
        private actroute: ActivatedRoute,
        private campsiteService: CampsiteService,
        private clientService: ClientService,
        private location: Location,
        private jhiAlertService: JhiAlertService,
        private router: Router
    ) {}

    ngOnInit() {
        this.actroute.queryParams.subscribe(qparam => {
            this.id = qparam.id;
            this.dateStart = moment(qparam.dateStart, 'YYYY-MM-DD');
            this.dateEnd = moment(qparam.dateEnd, 'YYYY-MM-DD');
        });
        this.campsiteService.find(this.id).subscribe(
            (res: HttpResponse<ICampsite>) => {
                this.accomodation = res.body;
            },
            (res: HttpErrorResponse) => this.jhiAlertService.error(res.message, null, null)
        );
        // TODO: Find the client with the username
        // this.clientService.find();
    }

    onSubmit() {}

    goBack(): void {
        this.location.back();
    }
}
