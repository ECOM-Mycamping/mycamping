import { IRent } from 'app/shared/model//rent.model';

export interface IClient {
    id?: number;
    firstName?: string;
    lastName?: string;
    userName?: string;
    password?: number;
    mail?: string;
    phone?: string;
    rents?: IRent[];
}

export class Client implements IClient {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public userName?: string,
        public password?: number,
        public mail?: string,
        public phone?: string,
        public rents?: IRent[]
    ) {}
}
