import { IDiscount } from 'app/shared/model//discount.model';
import { IRent } from 'app/shared/model//rent.model';
import { IOwner } from 'app/shared/model//owner.model';

export interface ICampsite {
    id?: number;
    camping?: string;
    location?: string;
    type?: string;
    number?: number;
    surface?: number;
    utilities?: string;
    price?: number;
    description?: string;
    succDiscount?: number;
    pathimg?: string;
    discounts?: IDiscount[];
    rents?: IRent[];
    owner?: IOwner;
}

export class Campsite implements ICampsite {
    constructor(
        public id?: number,
        public camping?: string,
        public location?: string,
        public type?: string,
        public number?: number,
        public surface?: number,
        public utilities?: string,
        public price?: number,
        public description?: string,
        public succDiscount?: number,
        public pathimg?: string,
        public discounts?: IDiscount[],
        public rents?: IRent[],
        public owner?: IOwner
    ) {}
}
