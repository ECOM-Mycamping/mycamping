import { Moment } from 'moment';
import { ICampsite } from 'app/shared/model//campsite.model';

export interface IDiscount {
    id?: number;
    percentage?: number;
    startDate?: Moment;
    endDate?: Moment;
    campsite?: ICampsite;
}

export class Discount implements IDiscount {
    constructor(
        public id?: number,
        public percentage?: number,
        public startDate?: Moment,
        public endDate?: Moment,
        public campsite?: ICampsite
    ) {}
}
