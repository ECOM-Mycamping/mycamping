import { ICampsite } from 'app/shared/model//campsite.model';

export interface IOwner {
    id?: number;
    firstName?: string;
    lastName?: string;
    userName?: string;
    password?: number;
    mail?: string;
    phone?: string;
    campsites?: ICampsite[];
}

export class Owner implements IOwner {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public userName?: string,
        public password?: number,
        public mail?: string,
        public phone?: string,
        public campsites?: ICampsite[]
    ) {}
}
