import { Moment } from 'moment';
import { ICampsite } from 'app/shared/model//campsite.model';
import { IClient } from 'app/shared/model//client.model';

export interface IRent {
    id?: number;
    startDate?: Moment;
    endDate?: Moment;
    totalDiscount?: number;
    campsite?: ICampsite;
    client?: IClient;
}

export class Rent implements IRent {
    constructor(
        public id?: number,
        public startDate?: Moment,
        public endDate?: Moment,
        public totalDiscount?: number,
        public campsite?: ICampsite,
        public client?: IClient
    ) {}
}
