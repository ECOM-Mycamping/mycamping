import { Route } from '@angular/router';

import { AccomodationComponent } from './';

export const ACCOMODATION_ROUTE: Route = {
    path: 'accomodation/:id',
    component: AccomodationComponent,
    data: {
        authorities: [],
        pageTitle: 'accomodation.title'
    }
};
