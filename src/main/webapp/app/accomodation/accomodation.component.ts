import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiAlertService } from 'ng-jhipster';
import { Location } from '@angular/common';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { CampsiteService } from '../entities/campsite/campsite.service';
import { ICampsite } from 'app/shared/model/campsite.model';
import { faCalendar, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';

@Component({
    selector: 'jhi-accomodation',
    templateUrl: './accomodation.component.html',
    styleUrls: ['accomodation.scss']
})
export class AccomodationComponent implements OnInit {
    id: number;
    accomodation: ICampsite;
    faCalendar = faCalendar;
    faArrow = faArrowCircleLeft;
    dateStart = null;
    dateEnd = null;

    constructor(
        private actroute: ActivatedRoute,
        private campsiteService: CampsiteService,
        private location: Location,
        private jhiAlertService: JhiAlertService,
        private router: Router
    ) {}

    ngOnInit() {
        this.id = Number(this.actroute.snapshot.paramMap.get('id'));
        this.actroute.queryParams.subscribe(qparam => {
            if (qparam.date) {
                this.dateStart = moment(qparam.dateStart);
                this.dateEnd = moment(qparam.dateEnd);
            }
        });
        this.campsiteService.find(this.id).subscribe(
            (res: HttpResponse<ICampsite>) => {
                this.accomodation = res.body;
            },
            (res: HttpErrorResponse) => this.jhiAlertService.error(res.message, null, null)
        );
    }

    onSubmit() {
        this.router.navigate(['/payment/'], { queryParams: { id: this.id, dateStart: this.dateStart._i, dateEnd: this.dateEnd._i } });
    }

    goBack(): void {
        this.location.back();
    }
}
