import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MycampingSharedModule } from 'app/shared';
import { ACCOMODATION_ROUTE, AccomodationComponent } from './';

@NgModule({
    imports: [MycampingSharedModule, RouterModule.forChild([ACCOMODATION_ROUTE])],
    declarations: [AccomodationComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MycampingAccomodationModule {}
