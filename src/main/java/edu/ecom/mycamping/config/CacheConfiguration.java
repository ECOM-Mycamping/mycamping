package edu.ecom.mycamping.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(edu.ecom.mycamping.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Client.class.getName(), jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Client.class.getName() + ".rents", jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Owner.class.getName(), jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Owner.class.getName() + ".campsites", jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Campsite.class.getName(), jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Campsite.class.getName() + ".discounts", jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Campsite.class.getName() + ".rents", jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Discount.class.getName(), jcacheConfiguration);
            cm.createCache(edu.ecom.mycamping.domain.Rent.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
