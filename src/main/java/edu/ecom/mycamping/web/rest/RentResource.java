package edu.ecom.mycamping.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ecom.mycamping.domain.Rent;
import edu.ecom.mycamping.repository.RentRepository;
import edu.ecom.mycamping.web.rest.errors.BadRequestAlertException;
import edu.ecom.mycamping.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Rent.
 */
@RestController
@RequestMapping("/api")
public class RentResource {

    private final Logger log = LoggerFactory.getLogger(RentResource.class);

    private static final String ENTITY_NAME = "rent";

    private final RentRepository rentRepository;

    public RentResource(RentRepository rentRepository) {
        this.rentRepository = rentRepository;
    }

    /**
     * POST  /rents : Create a new rent.
     *
     * @param rent the rent to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rent, or with status 400 (Bad Request) if the rent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rents")
    @Timed
    public ResponseEntity<Rent> createRent(@Valid @RequestBody Rent rent) throws URISyntaxException {
        log.debug("REST request to save Rent : {}", rent);
        if (rent.getId() != null) {
            throw new BadRequestAlertException("A new rent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Rent result = rentRepository.save(rent);
        return ResponseEntity.created(new URI("/api/rents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rents : Updates an existing rent.
     *
     * @param rent the rent to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rent,
     * or with status 400 (Bad Request) if the rent is not valid,
     * or with status 500 (Internal Server Error) if the rent couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rents")
    @Timed
    public ResponseEntity<Rent> updateRent(@Valid @RequestBody Rent rent) throws URISyntaxException {
        log.debug("REST request to update Rent : {}", rent);
        if (rent.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Rent result = rentRepository.save(rent);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rent.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rents : get all the rents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of rents in body
     */
    @GetMapping("/rents")
    @Timed
    public List<Rent> getAllRents() {
        log.debug("REST request to get all Rents");
        return rentRepository.findAll();
    }

    /**
     * GET  /rents/:id : get the "id" rent.
     *
     * @param id the id of the rent to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rent, or with status 404 (Not Found)
     */
    @GetMapping("/rents/{id}")
    @Timed
    public ResponseEntity<Rent> getRent(@PathVariable Long id) {
        log.debug("REST request to get Rent : {}", id);
        Optional<Rent> rent = rentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(rent);
    }

    /**
     * DELETE  /rents/:id : delete the "id" rent.
     *
     * @param id the id of the rent to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rents/{id}")
    @Timed
    public ResponseEntity<Void> deleteRent(@PathVariable Long id) {
        log.debug("REST request to delete Rent : {}", id);

        rentRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
