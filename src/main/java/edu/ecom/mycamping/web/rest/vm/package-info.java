/**
 * View Models used by Spring MVC REST controllers.
 */
package edu.ecom.mycamping.web.rest.vm;
