package edu.ecom.mycamping.web.rest;

import com.codahale.metrics.annotation.Timed;
import edu.ecom.mycamping.domain.Campsite;
import edu.ecom.mycamping.repository.CampsiteRepository;
import edu.ecom.mycamping.web.rest.errors.BadRequestAlertException;
import edu.ecom.mycamping.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


/**
 * REST controller for managing Campsite.
 */
@RestController
@RequestMapping("/api")
public class CampsiteResource {

    private final Logger log = LoggerFactory.getLogger(CampsiteResource.class);

    private static final String ENTITY_NAME = "campsite";

    private final CampsiteRepository campsiteRepository;

    public CampsiteResource(CampsiteRepository campsiteRepository) {
        this.campsiteRepository = campsiteRepository;
    }

    /**
     * POST  /campsites : Create a new campsite.
     *
     * @param campsite the campsite to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campsite, or with status 400 (Bad Request) if the campsite has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campsites")
    @Timed
    public ResponseEntity<Campsite> createCampsite(@Valid @RequestBody Campsite campsite) throws URISyntaxException {
        log.debug("REST request to save Campsite : {}", campsite);
        if (campsite.getId() != null) {
            throw new BadRequestAlertException("A new campsite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Campsite result = campsiteRepository.save(campsite);
        return ResponseEntity.created(new URI("/api/campsites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campsites : Updates an existing campsite.
     *
     * @param campsite the campsite to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campsite,
     * or with status 400 (Bad Request) if the campsite is not valid,
     * or with status 500 (Internal Server Error) if the campsite couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campsites")
    @Timed
    public ResponseEntity<Campsite> updateCampsite(@Valid @RequestBody Campsite campsite) throws URISyntaxException {
        log.debug("REST request to update Campsite : {}", campsite);
        if (campsite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Campsite result = campsiteRepository.save(campsite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campsite.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campsites : get all the campsites.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of campsites in body
     */
    @GetMapping("/campsites")
    @Timed
    public List<Campsite> getAllCampsites() {
        log.debug("REST request to get all Campsites");
        return campsiteRepository.findAll();
    }

    /**
     * GET  /campsites/:id : get the "id" campsite.
     *
     * @param id the id of the campsite to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/{id}")
    @Timed
    public ResponseEntity<Campsite> getCampsite(@PathVariable Long id) {
        log.debug("REST request to get Campsite : {}", id);
        Optional<Campsite> campsite = campsiteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(campsite);
    }
    
    /**
     * GET  /campsites/filterByLocation/:utilities get the campsite list filtered by Location
     *
     * @param location Required: the location of the campsite to retrieve.
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/filterByLocation")
    @Timed
    public List<Campsite> getCampsiteByLocation(@RequestParam String location) {
        return campsiteRepository.findByLocation(location);
    }
    
    /**
     * GET  /campsites/filterByLocation/:utilities get the campsite list filtered by type
     *
     * @param type Required: the type of the campsite to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/filterByType")
    @Timed
    public List<Campsite> getCampsiteByType(@RequestParam String type) {
        return campsiteRepository.findByType(type);
    }
    
    /**
     * GET  /campsites/filterByUtilities/:utilities get the campsite list filtered by utilities
     *
     * @param utility1 Required: First utility avaiable.
     * @param utility2 Second utility avaiable, must be higer than utility 1 in alphabetical order.
     * @param utility3 Third utility avaiable, must be higer than utility 1 and 2 in alphabetical order.
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/filterByUtilities")
    @Timed
    public List<Campsite> getCampsiteByUtilities(@RequestParam String utility1, @RequestParam(defaultValue="") String utility2, @RequestParam(defaultValue="") String utility3) {
        return campsiteRepository.findByUtilities(utility1,utility2,utility3);
    }

     /**
     * GET  /campsites/filterByDate/ get the campsite list filtered by date
     *
     * @param debut start date 
     * @param fin end date, muste be higher than debut
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/filterByDate")
    @Timed
    public List<Campsite> getCampsiteByDate(@RequestParam String dateDebut, @RequestParam String dateFin) {
        
        String[] tab = dateDebut.split("-");
        LocalDate debut = LocalDate.of(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]), Integer.parseInt(tab[2]));
        tab = dateFin.split("-");
        LocalDate fin = LocalDate.of(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]), Integer.parseInt(tab[2]));
        
        return campsiteRepository.findByDates(debut, fin);
    }

    /**
     * GET  /campsites/filterByAll
     *
     * @param debut start date 
     * @param fin end date, muste be higher than debut
     * @param utility1 First utility avaiable.
     * @param utility2 Second utility avaiable, must be higer than utility 1 in alphabetical order.
     * @param utility3 Third utility avaiable, must be higer than utility 1 and 2 in alphabetical order.
     * @param pricemin minimum price 
     * @param priceMax maximum price
     * @param surfaceMin minimum surface 
     * @param surfaceMax maximum surface
     * @param location the location of the campsite to retrieve.
     * @param type the type of the campsite to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/filterByAll")
    @Timed
    public List<Campsite> getCampsiteByAll(@RequestParam(defaultValue="1980-01-01") String dateDebut, @RequestParam(defaultValue="1980-01-02") String dateFin, @RequestParam(defaultValue="") String utility1, @RequestParam(defaultValue="") String utility2, @RequestParam(defaultValue="") String utility3, @RequestParam(defaultValue="0") Float priceMin, @RequestParam(defaultValue="500000") Float priceMax, @RequestParam(defaultValue="0") Integer surfaceMin, @RequestParam(defaultValue="500000") Integer surfaceMax, @RequestParam(defaultValue="") String location, @RequestParam(defaultValue="") String type) {
        String[] tab = dateDebut.split("-");
        LocalDate debut = LocalDate.of(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]), Integer.parseInt(tab[2]));
        tab = dateFin.split("-");
        LocalDate fin = LocalDate.of(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]), Integer.parseInt(tab[2]));

        if(utility1 == null)
            return campsiteRepository.findByDates(debut, fin);
        
        return campsiteRepository.findByAll(debut, fin, utility1, utility2, utility3, priceMin, priceMax, surfaceMin, surfaceMax, location, type);
    }

    /**
     * GET  /campsites/filterBySurface/ get the campsite list filtered by surface
     *
     * @param surfaceMin minimum surface 
     * @param surfaceMax maximum surface
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/filterBySurface")
    @Timed
    public List<Campsite> getCampsiteBySurface(@RequestParam Integer surfaceMin, @RequestParam Integer surfaceMax) {
        
        return campsiteRepository.findBySurface(surfaceMin, surfaceMax);
    }

/**
     * GET  /campsites/filterByPrice/ get the campsite list filtered by price
     *
     * @param pricemin minimum price 
     * @param priceMax maximum price
     * @return the ResponseEntity with status 200 (OK) and with body the campsite, or with status 404 (Not Found)
     */
    @GetMapping("/campsites/filterByPrice")
    @Timed
    public List<Campsite> getCampsiteByPrice(@RequestParam Float priceMin, @RequestParam Float priceMax) {
        
        return campsiteRepository.findByPrice(priceMin, priceMax);
    }

    /**
     * DELETE  /campsites/:id : delete the "id" campsite.
     *
     * @param id the id of the campsite to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campsites/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampsite(@PathVariable Long id) {
        log.debug("REST request to delete Campsite : {}", id);

        campsiteRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
