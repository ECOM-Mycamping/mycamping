package edu.ecom.mycamping.repository;

import edu.ecom.mycamping.domain.Campsite;

import java.util.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Campsite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampsiteRepository extends JpaRepository<Campsite, Long> {


    //Return a list of campsite filtered by utilities
	@Query("SELECT c FROM Campsite c WHERE c.location=?1")
    List<Campsite> findByLocation(String s);
	
	@Query("SELECT c FROM Campsite c WHERE c.type=?1")
    List<Campsite> findByType(String s);
	
    @Query("SELECT c FROM Campsite c WHERE c.utilities LIKE CONCAT('%',?1,'%',?2,'%',?3,'%')")
    List<Campsite> findByUtilities(String s1, String s2, String s3);

    ////Return a list of campsite filtered by dates 
    @Query("SELECT c FROM Campsite c WHERE c NOT IN "
    +"(SELECT r.campsite FROM Rent r WHERE "
    +"(r.startDate > ?1 AND (r.startDate < ?2 AND ?2 < r.endDate)) "
    +"OR ((r.startDate < ?1 AND ?1 < r.endDate) AND (r.startDate < ?2 AND ?2 < r.endDate)) "
    +"OR (r.startDate > ?1 AND ?2 > r.endDate) "
    +"OR ( r.endDate < ?2 AND (r.startDate < ?1 AND ?1 < r.endDate)))")
    List<Campsite> findByDates(LocalDate deb, LocalDate fin);

    //Mix of the 2 above
    @Query("SELECT c FROM Campsite c WHERE (c.utilities LIKE CONCAT('%',?3,'%',?4,'%',?5,'%')) AND ((c.surface >= ?8) AND (c.surface <= ?9)) AND ((c.price >= ?6) AND (c.price <= ?7)) AND (c.location LIKE CONCAT('%',?10,'%')) AND (c.type LIKE CONCAT('%',?11,'%')) AND (c NOT IN "
    +"(SELECT r.campsite FROM Rent r WHERE "
    +"(r.startDate > ?1 AND (r.startDate < ?2 AND ?2 < r.endDate)) "
    +"OR ((r.startDate < ?1 AND ?1 < r.endDate) AND (r.startDate < ?2 AND ?2 < r.endDate)) "
    +"OR (r.startDate > ?1 AND ?2 > r.endDate) "
    +"OR ( r.endDate < ?2 AND (r.startDate < ?1 AND ?1 < r.endDate))))")
    List<Campsite> findByAll(LocalDate deb, LocalDate fin, String s1, String s2, String s3, Float priceMin, Float priceMax, Integer surfaceMin, Integer surfaceMax, String location, String type);

    //Return a list of campsite filtered by surface
    @Query("SELECT c FROM Campsite c WHERE (c.surface >= ?1) AND (c.surface <= ?2)")
    List<Campsite> findBySurface(Integer surfaceMin, Integer surfaceMax);

    //Return a list of campsite filtered by price
    @Query("SELECT c FROM Campsite c WHERE (c.price >= ?1) AND (c.price <= ?2)")
    List<Campsite> findByPrice(Float priceMin, Float priceMax);
    
}    