package edu.ecom.mycamping.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Campsite.
 */
@Entity
@Table(name = "campsite")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Campsite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "camping")
    private String camping;

    @NotNull
    @Column(name = "location", nullable = false)
    private String location;

    @NotNull
    @Column(name = "jhi_type", nullable = false)
    private String type;

    @Column(name = "jhi_number")
    private Integer number;

    @NotNull
    @Column(name = "surface", nullable = false)
    private Integer surface;

    @Column(name = "utilities")
    private String utilities;

    @NotNull
    @Column(name = "price", nullable = false)
    private Float price;

    @Column(name = "description")
    private String description;

    @Column(name = "succ_discount")
    private Float succDiscount;

    @Column(name = "pathimg")
    private String pathimg;

    @OneToMany(mappedBy = "campsite")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Discount> discounts = new HashSet<>();
    @OneToMany(mappedBy = "campsite")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Rent> rents = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("campsites")
    private Owner owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCamping() {
        return camping;
    }

    public Campsite camping(String camping) {
        this.camping = camping;
        return this;
    }

    public void setCamping(String camping) {
        this.camping = camping;
    }

    public String getLocation() {
        return location;
    }

    public Campsite location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public Campsite type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNumber() {
        return number;
    }

    public Campsite number(Integer number) {
        this.number = number;
        return this;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSurface() {
        return surface;
    }

    public Campsite surface(Integer surface) {
        this.surface = surface;
        return this;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public String getUtilities() {
        return utilities;
    }

    public Campsite utilities(String utilities) {
        this.utilities = utilities;
        return this;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public Float getPrice() {
        return price;
    }

    public Campsite price(Float price) {
        this.price = price;
        return this;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public Campsite description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getSuccDiscount() {
        return succDiscount;
    }

    public Campsite succDiscount(Float succDiscount) {
        this.succDiscount = succDiscount;
        return this;
    }

    public void setSuccDiscount(Float succDiscount) {
        this.succDiscount = succDiscount;
    }

    public String getPathimg() {
        return pathimg;
    }

    public Campsite pathimg(String pathimg) {
        this.pathimg = pathimg;
        return this;
    }

    public void setPathimg(String pathimg) {
        this.pathimg = pathimg;
    }

    public Set<Discount> getDiscounts() {
        return discounts;
    }

    public Campsite discounts(Set<Discount> discounts) {
        this.discounts = discounts;
        return this;
    }

    public Campsite addDiscount(Discount discount) {
        this.discounts.add(discount);
        discount.setCampsite(this);
        return this;
    }

    public Campsite removeDiscount(Discount discount) {
        this.discounts.remove(discount);
        discount.setCampsite(null);
        return this;
    }

    public void setDiscounts(Set<Discount> discounts) {
        this.discounts = discounts;
    }

    public Set<Rent> getRents() {
        return rents;
    }

    public Campsite rents(Set<Rent> rents) {
        this.rents = rents;
        return this;
    }

    public Campsite addRent(Rent rent) {
        this.rents.add(rent);
        rent.setCampsite(this);
        return this;
    }

    public Campsite removeRent(Rent rent) {
        this.rents.remove(rent);
        rent.setCampsite(null);
        return this;
    }

    public void setRents(Set<Rent> rents) {
        this.rents = rents;
    }

    public Owner getOwner() {
        return owner;
    }

    public Campsite owner(Owner owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Campsite campsite = (Campsite) o;
        if (campsite.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campsite.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Campsite{" +
            "id=" + getId() +
            ", camping='" + getCamping() + "'" +
            ", location='" + getLocation() + "'" +
            ", type='" + getType() + "'" +
            ", number=" + getNumber() +
            ", surface=" + getSurface() +
            ", utilities='" + getUtilities() + "'" +
            ", price=" + getPrice() +
            ", description='" + getDescription() + "'" +
            ", succDiscount=" + getSuccDiscount() +
            ", pathimg='" + getPathimg() + "'" +
            "}";
    }
}
